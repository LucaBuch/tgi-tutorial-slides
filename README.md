# Theoretische Grundlagen der Informatik – Tutoriumsfolien

WICHTIG: Dieses Repository enthält Materialien, die ausschließlich für Tutoren der Veranstaltung „Theoretische Grundlagen der Informatik“ am KIT gedacht sind.
Daher dürfen diese Quellen und Rohmaterialien insbesondere nicht den Tutanden oder anderen Personen zugänglich gemacht werden!
Vielen Dank für eure Mitarbeit!

Diese Folien können sich im Laufe des Semesters noch ändern. Es lohnt sich, ab und zu zu pullen. ;-)
Ich freue mich auch immer über gute Pull-Requests falls ihr euch Aufgaben habt einfallen lassen oder das Layout o.Ä. verschönert habt.

### Verwendung
Zunächst muss eine aktuelle Version des KIT-Themes installiert werden, siehe dazu das KIT-Vorlagen-Archiv im Ordner `Documents`. Es wird zwar eine leicht modifizierte Variante des KIT-Themes verwendet, diese greift aber trotzdem noch auf gewisse KIT-Packages zu, da das vollständige Entfernen aller Abhängigkeiten ein hoher (unnötiger) Aufwand wäre.

Anschließend muss eine Kopie der `config_template.tex` als `config.tex` abgespeichert werden.
Bitte alle persönlichen Modifikationen in dieser `config.tex` vornehmen **und die Datei nicht ins Git einchecken**!

Zum bauen der UML-Diagramme mittels 'make-diagrams.sh' benötigt ihr eine lokale Installation von PlantUML.

Wenn ihr Fragen habt, stellt sie gerne im Tutoren-ILIAS im entsprechenden Thread oder direkt an Luca('luca.buchholz@student.kit.edu').

### FAQ
1. _Ich verwende eure Folien als Grundlage für meine eigenen. Ist das für euch ok?_  
 Ja, das Material darf beliebig weiterverwendet und modifiziert werden. Aber wenn du Fehler findest, sei so lieb und sag uns Bescheid. Danke! :o)
2. _Da sind komische LaTeX-Tags, die ich nicht kenne. Hilfe!_  
 Ja, Standard-LaTeX ist ziemlich mickrig und umständlich, weshalb wir es etwas aufgepeppt haben. :D  
 Viele Sachen sind selbsterklärend, wenn man sie im Einsatz sieht, ansonsten lohnt sich ein Blick in die `PraeambelTut.tex`. (Einige Dinge sind auch aus Herrn Worschs Feder übernommen worden, siehe `gbi-macros.tex`.)
3. _Was ist mit dem KIT-Logo?_  
 Das Logo kann aus dem Theme herausgenommen werden, da die offiziellen Logorichtlinien eine Verwendung durch Tutoren eigentlich nicht vorsehen und das Logo eine geschützte Marke ist. Wer es _unbedingt_ für nötig hält, sich daran halten zu müssen, kann in der `config.tex`-Datei die entsprechende Zeile anpassen. ;-) (Standardmäßig ist das Logo drin.)
4. _Ich will mithelfen? Was muss man denn zur Zeit noch machen?_
 Schau am besten mal in die 'TODO.txt'. Da sammel ich Dinge die ich gerne verbessern würde.
### Herkunft der Folien
Die Folien wurden ursprünglich von Thassilo Helmold und Daniel Jungkind im Rahmen der Vorlesung 'Grundbegriffe der Informatik' erstellt. Ihre Makros und Vorlagen wurden in diesen Foliensatz übernommen und an die Anforderungen in TGI angepasst.
